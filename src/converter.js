/**
 * Padding outputs 2 characters allways
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

const bad = (rgb) => {
    return (rgb === 0 ? toString(0) : toString(rgb))
}

module.exports = {
    /**
      * Converts RGB to Hex string
      * @param {number} red 0-255
      * @param {number} green 0-255
      * @param {number} blue 0-255
      * @returns {string} hex value
      */
    rgbToHex: (red,green,blue) => {
         const redHex = red.toString(16);
         const greenHex = green.toString(16);
         const blueHex = blue.toString(16);
         return pad(redHex) + pad(greenHex) + pad(blueHex);
    
    },

    hexToRgb: (hex) => {
        var red   = parseInt(hex.substring(0, 2), 16);
        var green = parseInt(hex.substring(2, 4), 16);
        var blue  = parseInt(hex.substring(4, 6), 16);

        return [red, green, blue];
    }
}